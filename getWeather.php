<?php

	require_once("dbconnect.php");

	$stationID = $_POST['stationID'];
	$num = $_POST['num'];
	$min = $_POST['min'];
	
	// table `wetherstationid`
	
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	$tbname = "wetherstationid";

	$sql = 'SELECT MAX(weather_id) as temp 	FROM wetherstationid Where station_id = "'.$stationID.'"';
	if(!$conn->query($sql)){
		die("Error:" .$conn->query($sql));		
	}
	$result = $conn->query($sql);	
	$temp = mysqli_fetch_array($result,MYSQLI_ASSOC);
	$max_id = $temp['temp'];
	
	$sql = "SELECT * FROM ".$tbname.' WHERE weather_id='.$max_id;	$result = $conn->query($sql);	$temp = mysqli_fetch_array($result, MYSQLI_ASSOC);
	$maxDateTime=$temp['create_date'];

	if($num==0) {
		$test = date_parse($temp['create_date']);
		$temp['year'] = $test["year"];
		$temp['month'] = $test["month"];
		$temp['day'] = $test["day"];
		$temp['hour'] = $test["hour"];
		$temp['min'] = $test["minute"];
		$temp['sec'] = $test["second"];

		echo json_encode($temp); 		
	}
	else{
		
		$interValMin = $min * $num;
		$sql = "SELECT subdate('".$maxDateTime."',INTERVAL ".$interValMin." MINUTE ) as datetime1"; 
		$result = $conn->query($sql);	
		$temp = mysqli_fetch_array($result, MYSQLI_ASSOC);
	
		$sql = "SELECT * from ".$tbname." where station_ID='".$stationID."' order by abs(TIMEDIFF(create_date, '".$temp['datetime1']."')) ASC LIMIT 1 ";
		$result = $conn->query($sql);	
		$temp = mysqli_fetch_array($result, MYSQLI_ASSOC);
	
		$test = date_parse($temp['create_date']);
	
		$temp['year'] = $test["year"];
		$temp['month'] = $test["month"];
		$temp['day'] = $test["day"];
		$temp['hour'] = $test["hour"];
		$temp['min'] = $test["minute"];
		$temp['sec'] = $test["second"];
		
		echo json_encode($temp);	
	}

	//var_dump($temp);
?>


