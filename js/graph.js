$(function(){	
	for(var i=nPoints-1; i>=0; i--){ // n points read
		for(var j =0; j<=geolocation.length-1; j++){
			getWeather(geolocation[j].stationID,i,30);
			var dd=new Date(content.year,content.month-1, content.day, content.hour, content.min, content.sec, 0);
			xAixsArr[j].push(dd);
			windSpeedArr[j].push(content.wind_speed);
			windDirArr[j].push(content.winddir);
			baroArr[j].push(content.pressure);
			maxIdArr[j]=content.weather_id;
		}
	}
	showWeather();
//	setInterval(showWeather, 300000);
});

function getRecentWeather(stationID){
	$.ajax({
		url:"getRecentWeather.php",
		dataType:'json',
		type:'post',
		data:{ 
			stationID:stationID
		},
		success:function(response){
			content = response;
		},
		async:false,
		error:function(response){
			alert("Error1!");
		}
	});
}
function getWeather(stationID, num, min){
	$.ajax({
		url:"getWeather.php",
		dataType:'json',
		type:'post',
		data:{
			stationID:stationID,
			num:num,
			min:min 
		},
		success:function(response){
			content = response;
		},
		async:false,
		error:function(response){
			alert("Error2!");
		}
	});
}
var geocoder;
var map,content;
var geolocation = new Array(
	{stationID:"IBURNETT2", name:"Burnett Heads",  lat:-24.763421, lng:152.415301},
	{stationID:"IQUEENSL61", name:"Maryborough",lat:-25.533452, lng:152.693773},
	{stationID:"IQUEENSL48", name:"Gympie",lat:-26.193210, lng:152.667734}
);

var xAixsArr=new Array(3); 		for(var i=0; i<=geolocation.length-1; i++)	xAixsArr[i]=new Array(); 
var windSpeedArr=new Array(3); 	for(var i=0; i<=geolocation.length-1; i++)	windSpeedArr[i]=new Array(); 
var windDirArr=new Array(3); 	for(var i=0; i<=geolocation.length-1; i++)	windDirArr[i]=new Array();  
var baroArr=new Array(3); 		for(var i=0; i<=geolocation.length-1; i++)	baroArr[i]=new Array();  
var maxIdArr=new Array(3);

var nPoints=8; // graph points

// setup initial map
function initialize() {
	geocoder		= new google.maps.Geocoder();	// create geocoder object
	var latLng		= new google.maps.LatLng(geolocation[1].lat, geolocation[1].lng);	// set default lat/long (new york city)
	var mapOptions	= {		// options for map
		zoom: 8,
		center: latLng
	}
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);	// create new map in the map-canvas div		
	showWeather();
}

function showWeather(){

	var marker, weather_content;

	for(var i =0; i<4; i++){
		var latLng	= new google.maps.LatLng(geolocation[i].lat, geolocation[i].lng);
		var marker=new google.maps.Marker({
			position:latLng,
			title:geolocation[i].name
		});

		marker.setMap(map);
		getRecentWeather(geolocation[i].stationID);
		
		var dateTime = content.stationTime.substr(0,5) + '-' + content.stationDate.substr(8,2)+'/'+ content.stationDate.substr(5,2)+'/'+ content.stationDate.substr(2,2);

		var html = '<div class="graph-box">'+
					'<b class="station-name" id="'+geolocation[i].stationID+'">'+
					geolocation[i].name+'</b>'+'<b class="pull-right">'+ dateTime+'</b><br>'+
					'<div id="wind-graph-container" class="wind-graph-container"></div>'+
					'<div id="barometer-graph-container" class="barometer-graph-container"></div>'+
					'</div>';
		$("#"+geolocation[i].stationID).parents(".gm-style-iw").parent().remove();
		var infowindow = new google.maps.InfoWindow({
			content:html
		});
		infowindow.open(map,marker);

		if(maxIdArr[i]!=content.weather_id){
			windSpeedArr[i].push(content.wind_speed);
			windDirArr[i].push(content.winddir);
			baroArr[i].push(content.pressure);
			maxIdArr[i]=content.weather_id;
		}
	}

	drawChart_wind();
	drawChart_baro();
}	

function drawChart_wind()
{
	var chart ;
	
	var nCurPoint=windSpeedArr[0].length;
	var jj=nCurPoint-nPoints; 

	chart = new CanvasJS.Chart("chartContainer1", {
	  //animationEnabled: true,
	  title:{
	    text: "Wind  (  "+content.create_date+"  ) "
	  },
	  axisX: {
	    //valueFormatString: "HH:mm"	    
	  },
	  axisY: {
	    title: "Wind",
	    includeZero: false,
	    suffix: " kmh"
	  },
	  legend:{
	    cursor: "pointer",
	    fontSize: 16,
	    itemclick: toggleDataSeriesWind
	  },
	  toolTip:{
	    shared: true
	  },
	  data: [{
	    name: "Burnett Heads",
	    type: "spline",  xValueType: "dateTime",
	    xValueFormatString: "DD MMM hh:mm TT",
	    yValueFormatString: "#0.## kmh",
	    showInLegend: true,
	    dataPoints: [
		  { x: new Date(xAixsArr[1][jj+0]), y: Number(windSpeedArr[1][jj+0]), indexLabel:windDirArr[0][jj+0] },
		  { x: new Date(xAixsArr[1][jj+1]), y: Number(windSpeedArr[1][jj+1]), indexLabel:windDirArr[0][jj+1] },
		  { x: new Date(xAixsArr[1][jj+2]), y: Number(windSpeedArr[1][jj+2]), indexLabel:windDirArr[0][jj+2] },
		  { x: new Date(xAixsArr[1][jj+3]), y: Number(windSpeedArr[1][jj+3]), indexLabel:windDirArr[0][jj+3] },
		  { x: new Date(xAixsArr[1][jj+4]), y: Number(windSpeedArr[1][jj+4]), indexLabel:windDirArr[0][jj+4] },
		  { x: new Date(xAixsArr[1][jj+5]), y: Number(windSpeedArr[1][jj+5]), indexLabel:windDirArr[0][jj+5] },
		  { x: new Date(xAixsArr[1][jj+6]), y: Number(windSpeedArr[1][jj+6]), indexLabel:windDirArr[0][jj+6] },
		  { x: new Date(xAixsArr[1][jj+7]), y: Number(windSpeedArr[1][jj+7]), indexLabel:windDirArr[0][jj+7] }
	    ]
	  }]
	});
	chart.render();
	
	function toggleDataSeriesWind(e){
		if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
			e.dataSeries.visible = false;
		}
		else{
			e.dataSeries.visible = true;
		}
		chart.render();
	};
}

function drawChart_baro()
{
	var chart ;
	var yy=new Array(3); 	for(var i=0; i<=2; i++) yy[i]=new Array();

	var nCurPoint=baroArr[0].length;
	var jj=nCurPoint-nPoints; 

	chart = new CanvasJS.Chart("chartContainer2", {
	  //animationEnabled: true,
	  title:{
	    text: "  Barometer(  "+content.create_date+"  )  "
	  },
	  axisX: {
	    valueFormatString: "HH:mm"
	  },
	  axisY: {
	    title: "Barometer",
	    includeZero: false,
	    suffix: " hPa"
	  },
	  legend:{
	    cursor: "pointer",
	    fontSize: 16,
	    itemclick: toggleDataSeriesBaro
	  },
	  toolTip:{
	    shared: true
	  },
	  data: [{
	    name: "Burnett Heads",
	    type: "spline",
	    xValueType: "dateTime",
	    //xValueFormatString: "DD MMM hh:mm TT",

	    yValueFormatString: "#0.## hPa",
	    showInLegend: true,
	    dataPoints: [
	      { x: new Date(xAixsArr[1][jj+0]), y: Number(baroArr[1][jj+0]), indexLabel:baroArr[0][jj+0] },
	  	  { x: new Date(xAixsArr[1][jj+1]), y: Number(baroArr[1][jj+1]), indexLabel:baroArr[0][jj+1] },
	      { x: new Date(xAixsArr[1][jj+2]), y: Number(baroArr[1][jj+2]), indexLabel:baroArr[0][jj+2] },
	      { x: new Date(xAixsArr[1][jj+3]), y: Number(baroArr[1][jj+3]), indexLabel:baroArr[0][jj+3] },
	      { x: new Date(xAixsArr[1][jj+4]), y: Number(baroArr[1][jj+4]), indexLabel:baroArr[0][jj+4] },
	      { x: new Date(xAixsArr[1][jj+5]), y: Number(baroArr[1][jj+5]), indexLabel:baroArr[0][jj+5] },
	      { x: new Date(xAixsArr[1][jj+6]), y: Number(baroArr[1][jj+6]), indexLabel:baroArr[0][jj+6] },
	      { x: new Date(xAixsArr[1][jj+7]), y: Number(baroArr[1][jj+7]), indexLabel:baroArr[0][jj+7] }
	    ]
	  }]
	});
	chart.render();
	
	function toggleDataSeriesBaro(e){
		if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
			e.dataSeries.visible = false;
		}
		else{
			e.dataSeries.visible = true;
		}
		chart.render();
	};
}
google.maps.event.addDomListener(window, 'load', initialize);	// setup initial map
setTimeout(function(){ 
	$("<span>Hello world!</span>").insertAfter(".graph-box");
}, 15000);
