$(function(){
	getWeatherCondition();
	setInterval(getWeatherCondition, 6000);
});
function getWeatherCondition(){
	var geolocation = new Array(
		{stationID:"IBURNETT2",name:"Burnett Heads",  lat:-24.763421, lng:152.415301},
		{stationID:"IQUEENSL61",name:"Maryborough",lat:-25.533452, lng:152.693773},
		{stationID:"IQUEENSL48",name:"Gympie",lat:-26.193210, lng:152.667734}
	);
	for(var i = 0;i<3;i++){
		stationID = geolocation[i].stationID;
		$.ajax({
			url:"getRecentWeather.php",
			dataType:'json',
			type:'post',
			data:{
				stationID:stationID
			},
			success:function(response){
				var html = '<p class="station-name text-center">'+geolocation[i].name+'</p>'+
					'<p class="box-temperature text-center">'+response.temperature+'°C</p>'+
					'<p class="humidity"><label>Humidity</label>'+response.humidity+'%</p>'+
					'<p class="wind"><label>Wind</label>'+"<font>(S)</font>"+response.wind_speed+"kph"+"<font>(D)</font>"+response.winddir+'</p>'+
					'<p class="wind-gust">'+"<font>(G)</font>"+response.wind_gust+"kph"+'</p>'+
					'<p class="rain"><label>Rain Since 0:00</label>'+response.raindaymm+'mm</p>'+
					'<p class="rain-last"><label>Last</label>'+response.rainhrmm+'mm</p>'+
					'<p class="pressure"><label>Pressure</label>'+response.pressure+'hPa</p>'+
					'<p class="uv-index"><label>Uv Index</label></p>'+
					'<p class="fire-danger"><label>Fire Danger</label></p>'+
					'<p class="date text-center">'+response.stationDate+" "+response.stationTime+'</p>';
				$("div#"+stationID).html(html);
			},
			async:false,
			error:function(response){
				alert("error");
			}

		});	
	}
}