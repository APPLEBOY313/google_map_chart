$(function(){	
	setInterval(showWeather, 60000);
});
function getRecentWeather(stationID){
	$.ajax({
		url:"getRecentWeather.php",
		dataType:'json',
		type:'post',
		data:{
			stationID:stationID
		},
		success:function(response){
			content = response;
		},
		async:false,
		error:function(response){
			alert("error");
		}

	});
}
	var geocoder;
	var map,content;
	var geolocation = new Array(
		{stationID:"IBURNETT2",name:"Burnett Heads",  lat:-24.763421, lng:152.415301},
		{stationID:"IQUEENSL61",name:"Maryborough",lat:-25.533452, lng:152.693773},
		{stationID:"IQUEENSL48",name:"Gympie",lat:-26.193210, lng:152.667734}
	);
	// setup initial map
	function initialize() {
		geocoder		= new google.maps.Geocoder();						// create geocoder object
		var latLng		= new google.maps.LatLng(geolocation[2].lat, geolocation[2].lng);		// set default lat/long (new york city)
		var mapOptions	= {													// options for map
			zoom: 8,
			center: latLng
		}
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);	// create new map in the map-canvas div		
		showWeather();
	}	
	function showWeather(){
		var marker, weather_content;
		for(var i =0;i<4;i++){
			latLng	= new google.maps.LatLng(geolocation[i].lat, geolocation[i].lng);
			var marker=new google.maps.Marker({
				position:latLng,
				title:geolocation[i].name
			});

			marker.setMap(map);
			getRecentWeather(geolocation[i].stationID);
			
			var dateTime = content.stationTime.substr(0,5) + '-' + content.stationDate.substr(8,2)+'/'+ content.stationDate.substr(5,2)+'/'+ content.stationDate.substr(2,2);

			var html = '<b class="station-name" id="'+geolocation[i].stationID+'">'+geolocation[i].name+'</b>'+'<b class="pull-right">'+ dateTime+'</b><br>'+
						'<p><label>Temp:</label>'+content.temperature+'°C<label>Humidity:</label>'+content.humidity+'%</p>'+
						'<p><label>Dew Point:</label>'+content.dew_point+'<label>Wind:</label>(s)'+content.wind_speed+'kmh'+'(D)'+content.winddir+"(G)"+content.wind_gust+'kmh</p>'+
						'<p><label>Rain 12am>:</label>'+content.raindaymm+"mm"+'<label>Pressure:</label>'+content.pressure+'hpa</p>';
			$("#"+geolocation[i].stationID).parents(".gm-style-iw").parent().remove();
			var infowindow = new google.maps.InfoWindow({
				content:html
			});
			infowindow.open(map,marker);

			html = '<td>'+geolocation[i].name+'</td>'+
					'<td>'+content.temperature+'</td>'+
					'<td>'+content.humidity+'</td>'+
					'<td>'+content.dew_point+'</td>'+
					'<td>'+content.wind_speed+'-'+content.winddir+'</td>'+
					'<td>'+content.raindaymm+'</td>'+
					'<td>'+content.pressure+'</td>'+
					'<td>'+content.stationDate+' '+content.stationTime+'</td>';
			var selector = "tr."+geolocation[i].stationID;
			$("tr#"+geolocation[i].stationID).html(html);
		}
	}	
	google.maps.event.addDomListener(window, 'load', initialize);		// setup initial map
	